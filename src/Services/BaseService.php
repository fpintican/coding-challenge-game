<?php

declare(strict_types=1);

namespace Ucc\Services;

use JsonMapper;
use JsonMapper_Exception;
use KHerGe\JSON\Exception\DecodeException;
use KHerGe\JSON\Exception\UnknownException;
use KHerGe\JSON\JSON;

abstract class BaseService
{
    /**
     * @param JSON $json
     * @param JsonMapper $jsonMapper
     * @param string $content
     * @param string $class
     * @return array of $class
     * @throws JsonMapper_Exception
     * @throws DecodeException
     * @throws UnknownException
     */
    protected function getModelCollection(JSON $json, JsonMapper $jsonMapper, string $content, string $class): array
    {
        $data = [];

        foreach ($json->decode($content) as $question) {
            $data[] = $jsonMapper->map($question, new $class());
        }

        return $data;
    }
}
