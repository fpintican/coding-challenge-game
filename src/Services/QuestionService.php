<?php

declare(strict_types=1);

namespace Ucc\Services;

use JsonMapper;
use JsonMapper_Exception;
use KHerGe\JSON\Exception\DecodeException;
use KHerGe\JSON\Exception\EncodeException;
use KHerGe\JSON\Exception\UnknownException;
use KHerGe\JSON\JSON;
use Ucc\Models\Question;
use Ucc\Session;

class QuestionService extends BaseService
{
    const QUESTIONS_PATH = __DIR__ . '/../../questions.json';

    /**
     * @var JSON
     */
    private JSON $json;

    /**
     * @var JsonMapper
     */
    private JsonMapper $jsonMapper;

    /**
     * @var Question[]
     */
    private array $questions;

    /**
     * @param JSON $json
     * @param JsonMapper $jsonMapper
     * @throws JsonMapper_Exception
     * @throws DecodeException
     * @throws UnknownException
     */
    public function __construct(JSON $json, JsonMapper $jsonMapper)
    {
        $this->json = $json;
        $this->jsonMapper = $jsonMapper;
        // @todo you can probably validate json schema
        $this->questions = $this->getModelCollection(
            $json,
            $jsonMapper,
            file_get_contents(self::QUESTIONS_PATH),
            Question::class
        );
        shuffle($this->questions);
    }

    /**
     * @param int $count
     * @return array
     */
    public function getRandomQuestions(int $count = 5): array
    {
        return array_slice($this->questions, 0, $count);
    }

    /**
     * @param int $id
     * @param string $answer
     * @return int
     */
    public function getPointsForAnswer(int $id, string $answer): int
    {
        /** @var Question|null $question */
        $question = current(
            array_filter($this->questions, function (Question $question) use ($id): bool {
                return $question->getId() === $id;
            })
        ) ?: null;

        if (null === $question) {
            return 0;
        }

        if ($question->getCorrectAnswer() !== $answer) {
            return 0;
        }

        return $question->getPoints();
    }

    /**
     * @param int $points
     * @return string
     */
    public function getExpectedQuestionMessage(int $points): string
    {
        if ($points === 0) {
            return 'Your answer is not correct';
        }

        return 'Your answer is correct';
    }

    /**
     * @param int $id
     * @return Question|null
     * @throws DecodeException
     * @throws UnknownException
     * @throws EncodeException
     */
    public function getNextQuestion(int $id): ?Question
    {
        $sessionQuestionIds = Session::get('question_ids') ?? '';
        $questionIds = array_merge($this->json->decode($sessionQuestionIds), [$id]);

        $question = current(
            array_filter(
                $this->questions,
                function (Question $question) use ($questionIds): bool {
                    return !in_array($question->getId(), $questionIds);
                }
            )
        ) ?: null;

        Session::set('question_ids', $this->json->encode($questionIds));

        return $question;
    }
}
