<?php

declare(strict_types=1);

namespace Ucc\Models;

use JsonSerializable;

class Question implements JsonSerializable
{
    private int $id;
    private string $question;
    private array $possibleAnswers;
    private string $correctAnswer;
    private int $points;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getQuestion(): string
    {
        return $this->question;
    }

    /**
     * @param string $question
     */
    public function setQuestion(string $question): void
    {
        $this->question = $question;
    }

    /**
     * @return array
     */
    public function getPossibleAnswers(): array
    {
        return $this->possibleAnswers;
    }

    /**
     * @param array $possibleAnswers
     */
    public function setPossibleAnswers(array $possibleAnswers): void
    {
        $this->possibleAnswers = $possibleAnswers;
    }

    /**
     * @return string
     */
    public function getCorrectAnswer(): string
    {
        return $this->correctAnswer;
    }

    /**
     * @param string $correctAnswer
     */
    public function setCorrectAnswer(string $correctAnswer): void
    {
        $this->correctAnswer = $correctAnswer;
    }

    /**
     * @return int
     */
    public function getPoints(): int
    {
        return $this->points;
    }

    /**
     * @param int $points
     */
    public function setPoints(int $points): void
    {
        $this->points = $points;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'question' => $this->question,
            'possibleAnswers' => $this->possibleAnswers,
            'points' => $this->points,
        ];
    }
}
