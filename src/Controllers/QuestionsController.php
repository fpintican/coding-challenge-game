<?php

declare(strict_types=1);

namespace Ucc\Controllers;

use Exception;
use Ucc\Http\JsonResponseTrait;
use Ucc\Services\QuestionService;
use Ucc\Session;

class QuestionsController extends Controller
{
    use JsonResponseTrait;

    /**
     * @var QuestionService
     */
    private QuestionService $questionService;

    public function __construct(QuestionService $questionService)
    {
        parent::__construct();
        $this->questionService = $questionService;
    }

    /**
     * @return void
     */
    public function beginGame(): void
    {
        $name = $this->requestBody->name ?? '';

        if (strlen($name) === 0) {
            $this->json(['You must provide a name'], 400);
            return;
        }

        Session::set('name', $name);
        Session::set('questionCount', '1');

        $question = current($this->questionService->getRandomQuestions()) ?: null;

        $this->json(['question' => $question], 201);
    }

    /**
     * @param int $id
     * @throws Exception
     * @return void
     */
    public function answerQuestion(int $id): void
    {
        if (null === Session::get('name')) {
            $this->json(['You must first begin a game'], 400);
        }

        $answer = $this->requestBody->answer ?? '';

        if (strlen($answer) === 0) {
            $this->json(['You must provide an answer'], 400);
            return;
        }

        if (Session::get('questionCount') > 4) {
            $name = Session::get('name');
            $points = (int) Session::get('points');
            $points += $this->questionService->getPointsForAnswer($id, $answer);
            Session::destroy();
            $this->json(['message' => "Thank you for playing {$name}. Your total score was: {$points} points!"]);
            return;
        }

        $points = $this->questionService->getPointsForAnswer($id, $answer);
        Session::set('questionCount', (string)(Session::get('questionCount') + 1));
        Session::set('points', (string)((int) Session::get('points') + $points));

        $this->json([
            'message' => $this->questionService->getExpectedQuestionMessage($points),
            'question' => $this->questionService->getNextQuestion($id),
            'points' => $points
        ]);
    }
}
