<?php

declare(strict_types=1);

namespace Ucc;

final class Session
{
    public static function start(): void
    {
        if (isset($_SERVER['HTTP_SESSION_ID'])) {
            session_id($_SERVER['HTTP_SESSION_ID']);
        }

        session_start();
    }

    public static function destroy(): void
    {
        session_unset();
        session_destroy();
    }

    /**
     * @param string $key
     * @param string $value
     * @return void
     */
    public static function set(string $key, string $value): void
    {
        $_SESSION[$key] = $value;
    }

    /**
     * @param string $key
     * @return string|null
     */
    public static function get(string $key): ?string
    {
        return $_SESSION[$key] ?? null;
    }
}
