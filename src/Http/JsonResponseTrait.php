<?php

declare(strict_types=1);

namespace Ucc\Http;

trait JsonResponseTrait
{
    /**
     * @param array|object $data
     * @param int $statusCode
     * @return void
     */
    public function json($data, int $statusCode = 200): void
    {
        http_response_code($statusCode);
        header('Session-Id: ' . session_id());
        header('Content-Type: application/json;charset=utf-8');

        $body = json_encode($data);

        if (json_last_error() === JSON_ERROR_NONE) {
            echo $body;
            return;
        }

        echo json_encode(json_last_error_msg() ?: '');
    }
}
